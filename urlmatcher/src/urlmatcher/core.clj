(ns urlmatcher.core
  (:require [clojure.string :as string]))


(def re-patterns {:host #"host\((.+?)\);"
                  :path #"path\((.+?)\);"
                  :params #"queryparam\((.+?)\);"})


(defn to-key [val]
  (when (and val string/starts-with? "?")
    (->> (re-find #"\?(\w+)" val)
         (second)
         (keyword))))


(defn keys-from-path [path]
  (when path
    (->> path (re-seq #"\?[^/]+") (map to-key))))


(defn path-pattern [pattern]
  (let [matcher (string/replace pattern #"\?[^/]+" "([^/]+)")]
    [(into [] (keys-from-path pattern))
   (re-pattern matcher)]))


(defn parse-path [pattern]
  (let [[_ path] (re-find (:path re-patterns) pattern)]
    (path-pattern path)))


(defn split-and-keywordize [param]
  (let [[name bind] (string/split param #"=")]
    [(keyword name) (to-key bind)]))


(defn parse-params [params]
  (let [parsed (re-seq (:params re-patterns) params)]
    (some->> parsed
             (map second)
             (map split-and-keywordize)
             (into [])
             (flatten)
             (apply hash-map))))


(defn new-pattern [pattern]
  (let [[_ host] (re-find (:host re-patterns) pattern)
        path (parse-path pattern)
        qparams  (parse-params pattern)]
    {:host host :path path :params qparams}))


(defn split-url [url]
  (let [u (java.net.URL. url)]
       [(.getHost u) (.getPath u) (.getQuery u)]))


(defn get-path-vals [urlpath {:keys [path]}]
  (when urlpath
    (let [[keys ptrn] path
          [_ & vals] (re-find ptrn urlpath)]
       (into [] (map vector keys vals)))))


(defn split-params [param]
 (let [[key val] (string/split param #"=")]
    [(keyword key) val]))


(defn split-url-query [query]
  (into [] (map split-params (string/split query #"&"))))


(defn get-query-vals [query {:keys [params]}]
  (when query (let [splitted (split-url-query query)]
              (into [] (filter some?
                               (map (fn [[k v]]
                                      (let [bind (k params)]
                                        (if bind
                                          [bind v]
                                          nil)))
                                    splitted))))))


(defn recognize [pattern url]
  (let [[host path query] (split-url url)]
    (when (= (:host pattern) host)
      (let [paths (get-path-vals path pattern)
            queries (get-query-vals query pattern)]
        (cond
         (and query (empty? queries)) nil
         (empty? paths) nil
         :else (into [] (apply concat [paths queries])))))))

