(ns urlmatcher.core-test
  (:require [clojure.test :refer :all]
            [urlmatcher.core :refer :all]))

(def dribbble (new-pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset);"))

(def dribbble2 (new-pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset); queryparam(list=?type);"))

(def twitter-url-brad "http://twitter.com/bradfitz/status/562360748727611392")


(deftest recognize-test
  (testing "twitter recognized"
    (let [twitter (new-pattern "host(twitter.com); path(?user/status/?id);")
          res (recognize twitter twitter-url-brad)]
      (is (= (frequencies res)
             (frequencies [[:id "562360748727611392"] [:user "bradfitz"]])))))

  (testing "dribble recognized"
    (is
     (= (recognize dribbble "https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users&offset=1")
        [[:id "1905065-Travel-Icons-pack"] [:offset "1"]])))

  (testing "dribble host mismatch"
    (is
     (= (recognize dribbble "https://twitter.com/shots/1905065-Travel-Icons-pack?list=users&offset=1")
        nil)))

  (testing "dribble offset queryparam missing"
    (is
     (= (recognize dribbble "https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users")
        nil)))
  )

(deftest testing-new-pattern
  (testing "create new-pattern"
      (let [dribl (new-pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset); queryparam(list=?type);")]
        (is
         (and
          (= (:host dribl) "dribbble.com")
          (= (:params dribl) {:offset :offset, :list :type})
          (= (first (:path dribl)) [:id])))
       )))

